# Alocai Backend Software Engineer code challenge.
## Scenario
Year 2271.  
German territory is occupied by Aliens.  
Playing video games is strictly banned because of the Entertainment Limitation For Humans Law.  
Video games black market prices gone crazy.

You're a hacker who has physical access to the old-fashioned Alien PC containing all games that have ever been
developed.  
You're about to download games with your old-fashioned pen-drive and sell them to the rich game collectors.  
There is one issue though: you'll know what's the available space of your pen-drive just 5 minutes before the action.  
So you need to find a way to know which games to pick basing on the pen-drive space to make this risky operation as much
profitable as possible.  
You wanna create a service which will return a combination of games with the highest possible
total value for the given pen-drive space and steal returned games from the computer.

## Table of content
# Standard
# Project Structure
# How to set up and Run


## Standard
-If using standard instructions, Python ≥ 3.8 is required.
-A virtual environment is recommended (like virtualenv==20.4.3).
-pip is required for installing software packages.
-It could be easily run on installation PostgreSQL.



## Project Structure
```
.
├── app
│   ├── api
│   │   ├── __init__.py
│   │   └── v1
│   │       ├── business.py
│   │       ├── __init__.py
│   │       ├── routes.py
│   │       ├── serializers.py
│   │       ├── views1.py
│   │       └── views.py
│   ├── games
│   │   ├── __init__.py
│   │   ├── models.py
│   │   ├── routes.py
│   │   └── views.py
│   ├── __init__.py
│   ├── templates
│   │   ├── game_doc.html
│   │   └── index.html
│   └── tests
│       ├── __init__.py
│       └── unit
│           ├── conftest.py
│           ├── flask_test.cfg
│           ├── __init__.py
│           ├── myapp.py
│           ├── test_api.py
│           └── test_models.py
├── config.py
├── main.py
├── migrations
│   ├── alembic.ini
│   ├── env.py
│   ├── README
│   ├── script.py.mako
│   └── versions
│       ├── 4eb4a2d4fc42_.py
│       └── __pycache__
│           └── 4eb4a2d4fc42_.cpython-38.pyc
├── readme.md
├── requiremnts.txt
├── run.py
└── WSGI.py
```



## How to set up and Run
1. Git clone the project.
```
git clone https://deepakcis@bitbucket.org/deepakcis/flask-test-job.git
```
2. Run project via docker compose
```
docker-compose up
```

## Api Structure
1. Api for database health check
```
    Type: GET
    Routes: /api/v1/status
    Description: This api is used for checking the database health if database is healthy it returns the response healthy and status code.
```

2. Api for create game
```
    Type: POST
    Routes: /api/v1/games
    Description: This post api is used for game creation it takes arguments like that:
            request = {"name":"testgame", "price":200.04, "space":11} 
            json=request
    so the routes will be-
    ('/api/v1/games', json=request)
    if game is saved than it will return the response-  args, 200
    here args are the arguents which is passed when the request is sent and 200 is status code
    if the game is not save than due to already existence it will response- {'message': 'Game already exist with this name.'}, 400
    where 400 is status code.
```

3. Api for best value game
```
    Type: POST
    Routes: '/api/v1/best_value_games?pen_drive_space=8897'
    Description: Here arguments in request is    request = {"pen_drive_space":'8897'} 
    This post api is used to get the best value games in pen_drive

    it will response if the pendrive space is not numeric and negative - {'message': 'Invalid space.'}, 400
    Else it will response {games, total_space, total_value, remaining_space, 200} 
```

4. Api for index view
```
    Type: GET
    Routes: /games/
    Description: This get api will response with simple html page
```

5. Api for docs
```
    Type: GET
    Routes: /games/docs/
    Description: This get api will response with docs of games
```