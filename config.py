import os

# Statement for enabling the development environment
DEBUG = True

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

'''Database cofigurations are defined '''
POSTGRES = {
    'user': 'flask_user',
    'pw': 'admin',
    'db': 'flask_db_test',
    'host': 'db',
    'port': '5432',
    }

SQLALCHEMY_DATABASE_URI = 'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES 
SQLALCHEMY_TRACK_MODIFICATIONS = True
DATABASE_CONNECT_OPTIONS = {}

''' Enable protection against *Cross-site Request Forgery (CSRF)*'''
CSRF_ENABLED = True

'''Use a secure, unique and absolutely secret key for
signing the data.'''
CSRF_SESSION_KEY = os.urandom(24)

'''Secret key for signing cookies'''
SECRET_KEY = os.urandom(24)

'''Admin credentials'''
ADMIN_CREDENTIALS = ('admin', 'admin')
FLASK_ADMIN_SWATCH = 'cerulean'
WTF_CSRF_ENABLED = False
