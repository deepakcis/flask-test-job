print("-=-=-=-=-=-=-=")
from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

'''Define the WSGI application object'''
app = Flask(__name__, instance_relative_config=True)
'''Configurations'''
app.config.from_object('config')
'''Define the database object'''
db = SQLAlchemy(app)
''' Migrate database '''
migrate = Migrate(app, db)

from app.games.models import Game
'''Routes are imported here for using as blueprint'''
from app.api.v1.routes import api as api_blueprint
from app.games.routes import games

'''Register blueprint(s)'''
app.register_blueprint(api_blueprint)
app.register_blueprint(games)