import app
from app import db, app
MAX_SPACE = 1073741824

class Game(db.Model):
    '''Model for Games'''
    __tablename__ = 'games'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), unique=True, nullable=False)
    price = db.Column(db.Float)
    space = db.Column(db.BigInteger)

    def __repr__(self):
        return '<Game %r>' % self.name


