from flask import Blueprint

from app import app
from app.games.views import *

games = Blueprint('games', __name__, url_prefix='/games')

'''urls for html doc'''
@app.route('/', methods=['GET'])
def index():
    return index_view()

@app.route('/docs', methods=['GET'])
def document():
    return game_doc_view()