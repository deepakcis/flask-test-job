from flask import render_template
from flask_restful import Resource
from app import app


def index_view():
    '''Function for rendering index '''
    return render_template('index.html')

def game_doc_view():
    '''Function for rendering game documents'''
    return render_template('game_doc.html')
