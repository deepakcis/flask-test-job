from flask import Blueprint
from flask_restful import Api

from app.api.v1.views import DatabaseStatusView, CreateGameView, GameValueView
'''Url prefix is defined for accessing api'''
api = Blueprint('api', __name__, url_prefix='/api/v1/')
rest_api = Api(api)
'''Url for accessing api'''
rest_api.add_resource(DatabaseStatusView, '/status')
rest_api.add_resource(CreateGameView, '/games')
rest_api.add_resource(GameValueView, '/best_value_games')