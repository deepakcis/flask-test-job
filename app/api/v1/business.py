from app import db, app
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine


def check_db_status():
    '''Function for checking database connection'''  
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    Session = sessionmaker(bind=engine)
    try:    
        conn = engine.connect()
        Session(bind=conn)
        return True
    except Exception as e:
        return False
