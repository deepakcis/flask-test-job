from unittest import result
from flask import Flask, make_response, jsonify, request, Response
from flask_restful import Resource, reqparse, abort 
from .serializers import CreateGameSerializer
from app.games.models import Game
from app.api.v1.business import check_db_status
from app import db

def positive(numeric_type):
    '''Function for checking positive values in fields'''
    def require_positive(value):
        number = numeric_type(value)
        if number <= 0:
            raise ArgumentTypeError(f"Number {value} must be positive.")
        return number

    return require_positive

'''Argument parser from request '''
data_parser = reqparse.RequestParser()
data_parser.add_argument("name", type=str, help="Game name is required", required=True)
data_parser.add_argument("price", type=positive(float), help="Price is required and must be positive numbers!", required=True)
data_parser.add_argument("space", type=positive(int), help="Space is required and must be positive numbers!", required=True)



class DatabaseStatusView(Resource):
    '''Class for appropriate status code and database connection health'''
    def get(self):
        '''get method for status code and database health'''
        db_status = check_db_status()
        if db_status:
            return make_response({"message":"healthy"}, 200)
        return make_response({"message":"Unhealthy"}, 400)

    def head(self):
        '''head method for status code and database health'''
        db_status = check_db_status()
        if db_status:
            return make_response({"message":"Healthy"}, 200)
        return make_response({"message":"Unhealthy"}, 400)
    
    
class CreateGameView(Resource):
    '''class for validating the payload and saves the game into the DB'''
    def post(self):
        request_data = request.get_json(force=True)
        name = request_data.get('name', None)
        price = request_data.get('price', None)
        space = request_data.get('space', None)

        args= data_parser.parse_args()   
        game = Game.query.filter_by(name=request_data.get('name')).first()

        if not game:
            post_data = Game(name=name,price=float(price), space=space)
            db.session.add(post_data)
            db.session.commit()
            # return True
            # return Response({"hello":"world"}, 200)
            return make_response(args, 200)
        
        return make_response({'message': 'Game already exist with this name.'}, 400)


class GameValueView(Resource):
    ''' class for  returning a combination of games that has the highest total value of all possible game combinations 
     that fits into given pen-drive space'''
    def post(self):
        total_space = 0
        total_value = 0
        final_data =[]
        request_data = request.args.get('pen_drive_space', None)
        '''Validating pen_drive_space query parameter'''
        if not request_data or not request_data.isnumeric():
            return make_response({'message': 'Invalid space.'}, 400)
        game_data = Game.query.filter(Game.space <=request_data).order_by(Game.space.desc())
        remaining_space = int(request_data)
        for game in game_data:
            total_space += game.space
            total_value += game.price
            final_data.append(game)

        res = CreateGameSerializer(many=True)
        result=res.dump(final_data)
        return make_response(jsonify({"games":result, "total_space": total_space, "total_value": round(total_value, 3),"remaining_space":remaining_space-total_space}), 200)


