from flask_marshmallow import Marshmallow
from app import app
from app.games.models import Game


ORM = Marshmallow(app)

class CreateGameSerializer(ORM.Schema):
    '''Serializer for serializing the game data'''
    class Meta:
        fields = ("name", "price", "space")
