from app.games.models import Game
from app import db
# from app import app
def test_new_game():
    '''Given a Game model 
    When a new game is creeated check the game name, price and space'''
    game = Game(name='God of War 3',price= 300,space=322)
    assert game.name == 'God of War 3'
    assert game.price == 300
    assert game.space == 322
    
 