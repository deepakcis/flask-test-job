import json
from app.games.models import Game
from app import db
from app import app


def test_create_game():
    '''Test cases for game creation api'''
    flask_app = app
    request = {"name":"testgame", "price":200.04, "space":11} 
    with app.test_client() as c:
        rv = c.post('/api/v1/games', json=request)
    print("here is the data", rv.json)
    
    assert rv.status_code == 200
    assert rv.json["name"] == request["name"]
    assert rv.json["price"] == request["price"]
    assert rv.json["space"] == request["space"]
    game = Game.query.filter_by(name=rv.json["name"]).first()
    db.session.delete(game)
    db.session.commit()

def test_best_value_game():
    '''Test cases for best value game api'''
    flask_app = app
    request = {"pen_drive_space":'8897'} 
    with app.test_client() as c:
        rv = c.post('/api/v1/best_value_games?pen_drive_space=8897')
    assert rv.status_code == 200